import pygame
import math
import time

screen_width = 800
screen_height = 600

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()
running = True

black = (0, 0, 0)
white = (255, 255, 255)

racket_width = 5
racket_height = 100
racket_color = white

racket_x = (screen_width - racket_width) // 2
racket_y = -20 + (screen_height - racket_height)

# Initial racket rotation angle (in degrees)
racket_angle = 0
rotation_speed = 1  # Speed of rotation

# Ball dimensions
ball_radius = 5
ball_color = white

ball_pos = pygame.math.Vector2(screen_width // 2, ball_radius)
ball_speed = pygame.math.Vector2(0, 2)

max_racket_speed = 3

ball_image = pygame.image.load('img/Tennis-ball.png')
ball_image = pygame.transform.scale(ball_image, (ball_radius * 4, ball_radius * 4))

# Countdown timer
countdown_start_time = None
countdown_duration = 3  # 3 seconds

def rotate_point(point, angle, origin):
    s, c = math.sin(angle), math.cos(angle)
    x, y = point[0] - origin[0], point[1] - origin[1]
    new_x = x * c - y * s + origin[0]
    new_y = x * s + y * c + origin[1]
    return new_x, new_y

hit_effect_color = (255, 0, 0)  # Red color

# Points counter
points = 0
myfont = pygame.font.Font(None, size=36)

# Function to draw the effect lines
def draw_effect_lines(center, radius, num_lines):
    angle_increment = 2 * math.pi / num_lines
    for i in range(num_lines):
        angle = i * angle_increment
        start_point = (
            center[0] + int(radius * math.cos(angle)),
            center[1] + int(radius * math.sin(angle))
        )
        end_point = (
            center[0] + int((radius + 5) * math.cos(angle)),
            center[1] + int((radius + 5) * math.sin(angle))
        )
        pygame.draw.line(screen, hit_effect_color, start_point, end_point, 2)

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Check if the countdown is done
    # Check for key presses
    keys = pygame.key.get_pressed()
    if keys[pygame.K_a]:
        racket_angle -= rotation_speed
    if keys[pygame.K_d]:
        racket_angle += rotation_speed

    # Limit the angle between 0 and 360 degrees
    racket_angle %= 360

    # Get mouse position
    mouse_x, _ = pygame.mouse.get_pos()

    desired_racket_x = mouse_x - racket_width // 2

    # Calculate the difference between current and desired racket positions
    delta_x = desired_racket_x - racket_x

    # Limit the change in racket_x based on max_racket_speed
    if delta_x > max_racket_speed:
        delta_x = max_racket_speed
    elif delta_x < -max_racket_speed:
        delta_x = -max_racket_speed

    # Update racket position
    racket_x += delta_x

    # Update ball position
    ball_pos += ball_speed

    # Ball collision with walls
    if ball_pos.x - ball_radius < 0 or ball_pos.x + ball_radius > screen_width:
        ball_speed.x *= -1

    # Update ball position
    time_step = 1  # Time step for continuous collision detection
    num_steps = int(ball_speed.length() * time_step)  # Number of steps based on ball speed
    for _ in range(num_steps):
        ball_pos += ball_speed * (time_step / num_steps)

    # Calculate rotated racket points
    origin = (racket_x + racket_width / 2, racket_y + racket_height / 2)
    p1 = rotate_point((racket_x, racket_y), math.radians(racket_angle), origin)
    p2 = rotate_point((racket_x + racket_width, racket_y), math.radians(racket_angle), origin)
    p3 = rotate_point((racket_x + racket_width, racket_y + racket_height), math.radians(racket_angle), origin)
    p4 = rotate_point((racket_x, racket_y + racket_height), math.radians(racket_angle), origin)

    # Check if ball is within rotated racket's bounding box
    min_x = min(p1[0], p2[0], p3[0], p4[0])
    max_x = max(p1[0], p2[0], p3[0], p4[0])
    min_y = min(p1[1], p2[1], p3[1], p4[1])
    max_y = max(p1[1], p2[1], p3[1], p4[1])
    
    if (
        min_x <= ball_pos.x - ball_radius <= max_x and
        min_y <= ball_pos.y - ball_radius <= max_y
    ):
        # Calculate the center of the racket
        racket_center = pygame.math.Vector2((min_x + max_x) / 2, (min_y + max_y) / 2)

    # Calculate the impact angle between the ball and racket
        impact_angle = math.atan2(ball_pos.y - racket_center.y, ball_pos.x - racket_center.x)

    # Calculate the difference between the racket's rotation and the impact angle
        angle_difference = math.radians(racket_angle) - impact_angle

    # Calculate the new ball speed after reflection
        new_ball_speed = ball_speed.length() * (1 + abs(math.cos(angle_difference)))

    # Calculate the new ball speed components based on the impact angle
        ball_speed.x = new_ball_speed * math.cos(impact_angle)
        ball_speed.y = new_ball_speed * math.sin(impact_angle)

    # Apply an additional vertical boost to simulate a more pronounced bounce
        vertical_boost = -0.2  # Adjust this value as needed
        ball_speed.y += vertical_boost

    # Ball out of bounds
    if ball_pos.y - ball_radius > screen_height or (ball_pos.y - ball_radius) < 0:
        ball_pos = pygame.math.Vector2(screen_width // 2, ball_radius)
        ball_speed = pygame.math.Vector2(0, 2)

    # Clear the screen
    screen.fill(black)

    rotated_racket = pygame.Surface((racket_width, racket_height), pygame.SRCALPHA)
    pygame.draw.rect(rotated_racket, racket_color, (0, 0, racket_width, racket_height))
    rotated_racket = pygame.transform.rotate(rotated_racket, racket_angle)
    rotated_racket_rect = rotated_racket.get_rect(center=(racket_x + racket_width / 2, racket_y + racket_height / 2))
    screen.blit(rotated_racket, rotated_racket_rect.topleft)

    # Display points counter
    points_text = f"Points: {points}"
    points_text_surface = myfont.render(points_text, True, white)
    points_text_rect = points_text_surface.get_rect(topright=(screen_width - 10, 10))
    screen.blit(points_text_surface, points_text_rect)
    
    # Draw the ball image
    ball_rect = ball_image.get_rect(center=(int(ball_pos.x), int(ball_pos.y)))
    screen.blit(ball_image, ball_rect)

    pygame.display.update()
    clock.tick(120)

pygame.quit()
