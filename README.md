# Infinite Racketeer

## Description
Infinite Racketeer is a game where there is no ending to the game. The player must continuously hit the ball back while simultaneously trying to hit specific targets to increase their points.  The player must avoid having the balls get past the racket as that decreases their “life”.

NOTICE: I may have misunderstood some of what I was supposed to build, so I may have went too far and built out the game further than what was expected. So, I have two files: game.py and prot.py which represent my final submission of the game and the prototype for the game respectively.

**Running the program**
Controls: Control the horizontal movement of the racket with your mouse. The racket will "follow" the mouse cursor.
Control the rotation of the racket with the "A" and "D" keys on the keyboard.
Run Program with: python3 game.py
Run the prototype with: python3 prot.py

**What was built** - 
The main things that were built were the mechanics of the game along with the design of how the game looked.
From the mechanics where there was collision with the ball and the racket and the walls of the game. The rotation of
the racket introduced a harder set of math to determine the correct collision properties.
A simple ball curvature mechanic was introduced depending on the racket movement.
Multiple targets are introduced as well to give it a better challenge for the user.
The racket controllability with the mouse and the keyboard.

**How it worked** -
At first the hardest parts were coding the physics of the game and designing the game. Sometimes
coding the collision became frustrating as I had to deal with the rotation of the racket as well. As it got further in the design process, the game became
more of a implementation of the other mechanics with multiple targets and etc.

**What didn't work** -
The main thing that comes to mind was the game proposal document was describing how I would try to do challenges during the game, but became something
that would take an extensive more amount of design and implementation. And for some other parts of the game, a more sophisticated ball curvature mechanic
became something that would take too much of time and design as well. So, all in all, the balancing portion of the game took the most amount of time when
creating it, even though I didn't touch much of that portion of the game.

**Lessons learned** -
Some lessons that I learned is how critically important it is to design more parts of your game before you start programming it. Even though programming it
as you go can seem fun, it usually ends up in a lot of frustration and rewriting of certain parts of the code. 

