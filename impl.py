import random
import pygame
import math

class target:
    def __init__(self, type, i_time, points, x, y, active, image_path):
        self.type = type
        self.points = points
        self.i_time = i_time
        self.x = x
        self.y = y
        self.active = active
        self.image = pygame.image.load(image_path)

class Ball:
    def __init__(self, pos, speed, radius, image, active, curve):
        self.pos = pygame.math.Vector2(pos.x, pos.y)
        self.speed = pygame.math.Vector2(speed.x, speed.y)
        self.curve = pygame.math.Vector2(curve.x, curve.y)
        self.radius = radius
        self.image = image
        self.active = active

target1 = target(0, 0, 1, 0, 0, 0, "img/Coin-S.png")
target2 = target(1, 0, 2, 0, 0, 0, "img/Coin-M.png")
target3 = target(2, 0, 3, 0, 0, 0, "img/Coin-L.png")
target4 = target(3, 0, -1, 0, 0, 0, "img/Spike-S.png")
target5 = target(4, 0, -2, 0, 0, 0, "img/Spike-M.png")
target6 = target(5, 0, -3, 0, 0, 0, "img/Spike-L.png")

black = (0, 0, 0)
white = (255, 255, 255)
red = (150, 0, 0) 

target_list = [target1, target2, target3, target4, target5, target6]

# Display game over
def display_end_screen(screen, screen_width, screen_height, myfont, points):
    screen.fill((0, 0, 0))

    end_text = "Game Over"
    points_text = f"Points: {points}"
    
    text_surface = myfont.render(end_text, True, (255, 255, 255))
    points_surface = myfont.render(points_text, True, (255, 255, 255))
    
    text_rect = text_surface.get_rect(center=(screen_width // 2, screen_height // 2 - 50))
    points_rect = points_surface.get_rect(center=(screen_width // 2, screen_height // 2))
    
    screen.blit(text_surface, text_rect)
    screen.blit(points_surface, points_rect)
    pygame.display.update()

# Display targets
def display_targets(screen, target):
    target_rect = target.image.get_rect(center=(int(target.x), int(target.y)))
    screen.blit(target.image, target_rect)

# Manage targets with time
def update_target(target, elapsed_time, countdown_time):
    if (countdown_time - target.i_time) > 8:
        target.active = 0
        target.i_time = 0
    if time_difficulty(countdown_time) == 0 and countdown_time > 3 and target.i_time == 0:
        if elapsed_time > 2:
            target = rand_target(target)
            target.i_time = countdown_time
            target.active = 1
            return 1
    elif time_difficulty(countdown_time) == 1 and target.i_time == 0:
        if elapsed_time > 1.5:
            target = rand_target(target)
            target.i_time = countdown_time
            target.active = 1
            return 1
    elif time_difficulty(countdown_time) == 2 and target.i_time == 0:
        if elapsed_time > 1:
            target = rand_target(target)
            target.i_time = countdown_time
            target.active = 1
            return 1
    elif time_difficulty(countdown_time) == 3 and target.i_time == 0:
        if elapsed_time > 0.5:
            target = rand_target(target)
            target.i_time = countdown_time
            target.active = 1
            return 1
    return 0

# Manage balls with time
def ball_time_manage(ball, elapsed_time, countdown_time):
    if time_difficulty(countdown_time) == 0 and countdown_time > 5 and ball.active == 0:
        if elapsed_time > 2:
            ball.pos = rand_ball(ball.radius)
            ball.speed = pygame.math.Vector2(0, 2)
            ball.active = 1
            return 1
    elif time_difficulty(countdown_time) == 1 and ball.active == 0:
        if elapsed_time > 1.5:
            ball.pos = rand_ball(ball.radius)
            ball.speed = pygame.math.Vector2(0, 2)
            ball.active = 1
            return 1
    elif time_difficulty(countdown_time) == 2 and ball.active == 0:
        if elapsed_time > 1:
            ball.pos = rand_ball(ball.radius)
            ball.speed = pygame.math.Vector2(0, 2)
            ball.active = 1
            return 1
    elif time_difficulty(countdown_time) == 3 and ball.active == 0:
        if elapsed_time > 0.5:
            ball.pos = rand_ball(ball.radius)
            ball.speed = pygame.math.Vector2(0, 2)
            ball.active = 1
            return 1
    return 0

# Display countdown of game
def display_countdown(screen, countdown_duration, elapsed_time, screen_width, screen_height):
    countdown_text = str(int(countdown_duration - elapsed_time) + 1)
    myfont = pygame.font.Font(None, 100)
    text = myfont.render(countdown_text, True, black)
    text_rect = text.get_rect(center=(screen_width // 2, screen_height // 2))
    screen.blit(text, text_rect)
    pygame.display.update()

# Display main portion of game
def display_game(screen, background_image, racket_width, racket_height, racket_image, racket_angle, racket_x, racket_y, screen_width, points, lives, myfont):
    screen.blit(background_image, (0, 0))
    rotated_racket = pygame.Surface((racket_width, racket_height), pygame.SRCALPHA)

    # Draw the rotated racket image
    rotated_racket = pygame.transform.rotate(racket_image, racket_angle)
    rotated_racket_rect = rotated_racket.get_rect(center=(racket_x + racket_width / 2, racket_y + racket_height / 2))
    screen.blit(rotated_racket, rotated_racket_rect.topleft)

    # Display points counter
    points_text = f"Points: {points}"
    points_text_surface = myfont.render(points_text, True, black)
    points_text_rect = points_text_surface.get_rect(topright=(screen_width - 10, 10))
    screen.blit(points_text_surface, points_text_rect)

    # Draw the lives count
    lives_text = myfont.render(f"Lives: {lives}", True, red)
    screen.blit(lives_text, (screen_width - 790, 10))

def display_balls(screen, ball, ball_image):
    # Draw the ball image
    ball_rect = ball_image.get_rect(center=(int(ball.pos.x), int(ball.pos.y)))
    screen.blit(ball_image, ball_rect)

# Calculate reflection of the ball based on racket status
def calculate_reflection(ball, racket_angle, delta_x, y_curve_strength, x_curve_strength):
    # Calculate the angle of reflection based on the racket angle and the ball's velocity
    angle_of_reflection = math.atan2(-ball.speed.y, ball.speed.x) - math.radians(racket_angle - 90)

    # Calculate the magnitude of the ball's velocity after reflection
    new_speed = ball.speed.length() * (1 + abs(math.cos(angle_of_reflection)))

    # Calculate the new velocity components after reflection
    ball.speed.x = new_speed * math.cos(angle_of_reflection)
    ball.speed.y = new_speed * math.sin(angle_of_reflection)

    vertical_boost = -0.2
    ball.speed.y += vertical_boost
    if delta_x > 0:
        ball.curve.y = -0.02
        ball.curve.x = -0.02
    if  delta_x < 0:
        ball.curve.y = -0.02
        ball.curve.x = 0.02
    return ball

# Get collision racket box
def calculate_racket_box(p1, p2, p3, p4):
    min_x = min(p1[0], p2[0], p3[0], p4[0])
    max_x = max(p1[0], p2[0], p3[0], p4[0])
    min_y = min(p1[1], p2[1], p3[1], p4[1])
    max_y = max(p1[1], p2[1], p3[1], p4[1])
    return min_x, max_x, min_y, max_y

# Calculate the rotation point of the racket
def rotate_point(point, angle, origin):
    s, c = math.sin(angle), math.cos(angle)
    x, y = point[0] - origin[0], point[1] - origin[1]
    new_x = x * c - y * s + origin[0]
    new_y = x * s + y * c + origin[1]
    return new_x, new_y

# Calculate the points of the racket for collision
def calculate_racket_points(racket_angle, racket_x, racket_y, racket_width, racket_height):
    origin = (racket_x + racket_width / 2, racket_y + racket_height / 2)
    p1 = rotate_point((racket_x, racket_y), math.radians(racket_angle), origin)
    p2 = rotate_point((racket_x + racket_width, racket_y), math.radians(racket_angle), origin)
    p3 = rotate_point((racket_x + racket_width, racket_y + racket_height), math.radians(racket_angle), origin)
    p4 = rotate_point((racket_x, racket_y + racket_height), math.radians(racket_angle), origin)
    return origin, p1, p2, p3, p4

# Update ball speed
def update_ball(ball):
    ball.speed.y += ball.curve.y
    ball.speed.x += ball.curve.x
    ball.pos += ball.speed
    return ball.pos, ball.speed

# Limit racket speed
def limit_racket(max_racket_speed, delta_x):
    if delta_x > max_racket_speed:
        delta_x = max_racket_speed
    elif delta_x < -max_racket_speed:
        delta_x = -max_racket_speed
    return delta_x

# Rotate racket based on key presses
def rotate_racket(keys, rotation_speed, racket_angle):
    if keys[pygame.K_a]:
        racket_angle -= rotation_speed
    if keys[pygame.K_d]:
        racket_angle += rotation_speed
    return racket_angle

# Get a random target
def rand_target(target): 
    
    select_target = random.randint(0, 5)
    target_list[select_target] = target
    target.x = random.randint(0, 800)
    target.y = random.randint(0, 445)
    return target
        
# Get difficulty of game as time increases
def time_difficulty(time):
    if time < 15:
        return 0
    if time > 15:
        return 1
    if time > 60:
        return 2
    if time > 120:
        return 3

# Get a random ball starting at the top of the court
def rand_ball(ball_radius):
    target_x = random.randint(0, 800)
    return pygame.Vector2(target_x, ball_radius)

# Calculate the horizontal curve factor based on ball's distance from racket's center
def calculate_curve_factor(ball_x, racket_center_x, max_curve):
    distance = abs(ball_x - racket_center_x)
    normalized_distance = distance / (5 / 2)
    return normalized_distance * max_curve