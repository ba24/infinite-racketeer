"""
Author: Ben Anderson
Name: Infinite Racketeer
Description: A game of infinity until you run out of lives and gather points until you run out of lives.
"""

import math
import time
import sys
import impl
import pygame
import random

screen_width = 800
screen_height = 600

pygame.init()
screen = pygame.display.set_mode((screen_width, screen_height))
clock = pygame.time.Clock()
running = True

racket_width = 5
racket_height = 100

# Load and scale the background image
background_image = pygame.image.load('img/Court.png') 
background_image = pygame.transform.scale(background_image, (screen_width, screen_height))

racket_image = pygame.image.load('img/Racket.png')
racket_image = pygame.transform.scale(racket_image, (racket_width, racket_height))

racket_x = (screen_width - racket_width) // 2
racket_y = -20 + (screen_height - racket_height)

# Initial racket rotation angle (in degrees)
racket_angle = 90
rotation_speed = 1

# Ball dimensions
ball_radius = 5

max_racket_speed = 5.5

ball_image = pygame.image.load('img/Tennis-ball.png')
ball_image = pygame.transform.scale(ball_image, (ball_radius * 4, ball_radius * 4))

myfont = pygame.font.Font(None, 36)

balls = []
targets = []

# Create initial balls
for _ in range(3): 
    pos = (pygame.Vector2(400, ball_radius))
    speed = pygame.math.Vector2(0, 2)
    curve = pygame.math.Vector2(0, 0)
    balls.append(impl.Ball(pos, speed, ball_radius, ball_image, 0, curve))

# Create initial targets
for _ in range(20):
    random_num = random.randint(0, 5)
    targets.append(impl.target_list[random_num])

# Countdown timer
countdown_start_time = None
ball_start_time = None
target_start_time = None
countdown_duration = 3

# Points counter
points = 0

# time for end of game
t = 0 

y_curve_strength = 0
x_curve_strength = 0

lives = 3
ball_run_true = 0

while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
    
    # Check for key presses
    keys = pygame.key.get_pressed()
    if keys[pygame.K_ESCAPE]:
        pygame.quit()
        sys.exit()

    # Start the countdowns
    if countdown_start_time is None:
        countdown_start_time = time.time()

    if ball_start_time is None:
        ball_start_time = time.time()

    if target_start_time is None:
        target_start_time = time.time()

    # Calculate elapsed time
    elapsed_time = time.time() - countdown_start_time
    ball_time = time.time() - ball_start_time
    target_time = time.time() - target_start_time
        
    # Check if the countdown is done
    if elapsed_time >= countdown_duration:
        # Check for key presses
        racket_angle = impl.rotate_racket(pygame.key.get_pressed(), rotation_speed, racket_angle)
        if racket_angle > 180:
            racket_angle = 0
        if racket_angle < 0:
            racket_angle = 180
    else:
        balls[0].pos = pygame.Vector2(400, 10)
        balls[0].active = 1

    # Get mouse position
    mouse_x, _ = pygame.mouse.get_pos()

    desired_racket_x = mouse_x - racket_width // 2

    # Calculate the difference between current and desired racket positions
    delta_x = desired_racket_x - racket_x

    # Limit the change in racket_x based on max_racket_speed
    delta_x = impl.limit_racket(max_racket_speed, delta_x)

    # Update racket position
    racket_x += delta_x

    # Update ball position
    for ball in balls:
        ball.pos, ball.speed = impl.update_ball(ball)

    for target in targets:
        if impl.update_target(target, target_time, elapsed_time) == 1:
            target_start_time = time.time()
            break

    # Ball collision with walls
    for ball in balls:
        if ball.pos.x - ball.radius < 0 or ball.pos.x + ball.radius > screen_width:
            ball.speed.x *= -1

    # Update ball position
    for ball in balls:
        time_step = 1  # Time step for continuous collision detection
        num_steps = int(ball.speed.length() * time_step)  # Number of steps based on ball speed
        for _ in range(num_steps):
            ball.pos += ball.speed * (time_step / num_steps)

    # Calculate rotated racket points
    origin, p1, p2, p3, p4 = impl.calculate_racket_points(racket_angle, racket_x, racket_y, racket_width, racket_height)
    
    # Check if ball is within rotated racket's bounding box
    min_x, max_x, min_y, max_y = impl.calculate_racket_box(p1, p2, p3, p4)
    
    for ball in balls:
        if (
            min_x <= ball.pos.x - ball.radius <= max_x and
            min_y <= ball.pos.y - ball.radius <= max_y
        ):
            ball = impl.calculate_reflection(ball, racket_angle, delta_x, y_curve_strength, x_curve_strength)

    for ball in balls:
        if impl.ball_time_manage(ball, ball_time, elapsed_time) == 1:
            ball_start_time = time.time()
            break
        else:
            continue      

    for ball in balls:
        
        if ball.pos.y - ball.radius > screen_height or ball.pos.y - ball.radius < 0:
            if ball.pos.y - ball.radius > screen_height:
                if ball.active == 1:
                    lives -= 1
            ball.active = 0
            ball.curve = pygame.Vector2(0, 0)
            if lives == 0:
                if t == 0:
                    t = elapsed_time
                while elapsed_time - t < 3:
                    impl.display_end_screen(screen, screen_width, screen_height, myfont, points)
                    elapsed_time = time.time() - countdown_start_time
                running = False

    if elapsed_time < countdown_duration:
        impl.display_countdown(screen, countdown_duration, elapsed_time, screen_width, screen_height)

    impl.display_game(screen, background_image, racket_width, racket_height, racket_image, racket_angle, racket_x, racket_y, screen_width, points, lives, myfont)

    for ball in balls:
        if ball.active == 1:
            impl.display_balls(screen, ball, ball_image)

    for target in targets:
        if target.active == 1:
            impl.display_targets(screen, target)

    for ball in balls:
        for target in targets:
            if target.active == 1 and ball.active == 1:
                if ball.pos.distance_to((target.x, target.y)) - 7 <= ball.radius:
                    points += target.points
                    target.active = 0  
                    ball.active = 0  
        
    pygame.display.update()
    clock.tick(120)

pygame.quit()